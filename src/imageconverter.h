#ifndef IMAGECONVERTER_H
#define IMAGECONVERTER_H

#include <QString>
#include <QStringList>


class ImageConverter
{

public:
    static QString convert(QString original);
    static void convert(QStringList *originals);

protected:
    QString convertToTemp(QString original);


private:
    ImageConverter();
    bool runConvert(QString source, QString destination);

};

#endif // IMAGECONVERTER_H
