<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>MultipleFileSelector</name>
    <message>
        <location filename="../../src/multiplefileselector.cpp" line="74"/>
        <source>Add files</source>
        <translation>Dateien hinzufügen</translation>
    </message>
    <message>
        <location filename="../../src/multiplefileselector.cpp" line="75"/>
        <source>Remove file</source>
        <translation>Datei entfernen</translation>
    </message>
    <message>
        <location filename="../../src/multiplefileselector.cpp" line="76"/>
        <source>Move file up</source>
        <translation>Datei nach vorne verschieben</translation>
    </message>
    <message>
        <location filename="../../src/multiplefileselector.cpp" line="77"/>
        <source>Move file down</source>
        <translation>Datei nach hinten verschieben</translation>
    </message>
    <message>
        <location filename="../../src/multiplefileselector.cpp" line="78"/>
        <source>Clear file list</source>
        <translation>Dateiliste leeren</translation>
    </message>
    <message>
        <location filename="../../src/multiplefileselector.cpp" line="106"/>
        <source>Open merge documents</source>
        <translation>Dateien öffnen</translation>
    </message>
    <message>
        <location filename="../../src/multiplefileselector.cpp" line="106"/>
        <source>PDF documents (*.pdf)</source>
        <translation>PDF-Dokumente (*.pdf)</translation>
    </message>
</context>
<context>
    <name>PdfMerger</name>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="76"/>
        <source>Merge</source>
        <translation>Zusammenhängen</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="81"/>
        <source>Persist file list</source>
        <translation>Dateiliste merken</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="94"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="130"/>
        <source>Discard list</source>
        <translation>Änderungen verwerfen</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="130"/>
        <source>The documents have not been merged yet. Do you want to quit anyway?</source>
        <translation>Die Dokumente wurden noch nicht zusammengeführt. Trotzdem beenden?</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="139"/>
        <source>Save merged document</source>
        <translation>Dokument speichern</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="139"/>
        <source>PDF document (*.pdf)</source>
        <translation>PDF-Dokument (*.pdf)</translation>
    </message>
    <message>
        <source>Merge documents</source>
        <translation type="vanished">Dokumente zusammenhängen</translation>
    </message>
    <message>
        <source>Document has been created.</source>
        <translation type="vanished">Dokument wurde erzeugt.</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="185"/>
        <source>pdfunite could not be called.</source>
        <translation>pdfunite konnte nicht aufgerufen werden.</translation>
    </message>
    <message>
        <source>Document could not be created. Error:
</source>
        <translation type="vanished">Dokument konnte nicht erzeugt werden. Fehler:
</translation>
    </message>
    <message>
        <source>Document could not be created.</source>
        <translation type="vanished">Dokument konnte nicht erzeugt werden.</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.cpp" line="221"/>
        <source>About PDF Merger</source>
        <translation>Über PDF Merger</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.h" line="23"/>
        <source>&lt;b&gt;PDF Merger&lt;/b&gt;&lt;br&gt;</source>
        <translation>&lt;b&gt;PDF Merger&lt;/b&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.h" line="24"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.h" line="25"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.h" line="26"/>
        <source>&lt;p&gt;PDF Merger is a simple graphical frontend for Popplers&apos; pdfunite command line tool. If poppler is not installed, this application is of no use.&lt;/p&gt;</source>
        <translation>&lt;p&gt;PDF Merger ist eine einfache grafische Oberfläche für Popplers Kommandozeilenprogramm pdfunite. Wenn Poppler nicht installiert ist, kann diese Anwendung nicht genutzt werden.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../src/pdfmerger.h" line="27"/>
        <source>&lt;p&gt;This application is published under the terms of the GNU General Public Licence v3 and comes as is without any warranties.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Dieses Programm wird unter den Bedingungen der GNU General Public Licence v3 veröffentlicht. Es wird keinerlei Gewährleistung oder Haftung übernommen.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>SuccessHandler</name>
    <message>
        <location filename="../../src/successhandler.cpp" line="151"/>
        <source>Document has been created.</source>
        <translation>Dokument wurde erzeugt.</translation>
    </message>
    <message>
        <location filename="../../src/successhandler.cpp" line="146"/>
        <source>Merge documents</source>
        <translation>Dokumente zusammenhängen</translation>
    </message>
    <message>
        <location filename="../../src/successhandler.cpp" line="31"/>
        <source>Document could not be created. Error:
</source>
        <translation>Dokument konnte nicht erzeugt werden. Fehler:
</translation>
    </message>
    <message>
        <location filename="../../src/successhandler.cpp" line="67"/>
        <source>Show file in Dolphin</source>
        <translation>Datei in Dolphin zeigen</translation>
    </message>
    <message>
        <location filename="../../src/successhandler.cpp" line="70"/>
        <source>Show file in Nautilus</source>
        <translation>Datei in Nautilus zeigen</translation>
    </message>
    <message>
        <location filename="../../src/successhandler.cpp" line="73"/>
        <source>Show file in Explorer</source>
        <translation>Datei in Explorer zeigen</translation>
    </message>
    <message>
        <location filename="../../src/successhandler.cpp" line="83"/>
        <source>Open file</source>
        <translation>Datei öffnen</translation>
    </message>
</context>
</TS>
